package functions;

public class SumFunction {

    public static void main(String[] args) {
        int resultSum = sumTwoNumbers(10, 15);
        System.out.println(resultSum);
    }

    public static int sumTwoNumbers(int numberOne, int numberTwo) {
        if (numberOne < numberTwo) {
            return 0;
        }
        //return numberOne + numberTwo;
        int resultSum = numberOne + numberTwo;
        return resultSum;
    }
}
